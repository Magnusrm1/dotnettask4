﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks.Dataflow;

namespace DotNetTask4
{
    class Program
    {
        public static List<string> names = new List<string>() { "Per Person", "Ola Nordmann", "Lise Olsen",
                                                                "Kari Nordmann", "Petter Skjerven"};

        // Lists used to temporarily store search hits.
        public static List<string> partialMatch = new List<string>();
        public static List<string> fullMatch = new List<string>();

        #region Square methods
        // Methods returns string user input of length and height respectively in string format.
        public static string GetLength()
        {
            Console.WriteLine("Write a number to choose the length of your square.");
            return Console.ReadLine();
        }
        public static string GetHeight()
        {
            Console.WriteLine("Write a number to choose the height of your square.");
            return Console.ReadLine();
        }

        public static void PrintSquare(int length, int height)
        {
            // Checking if the values entered are negative.
            if (length < 0 || height < 0)
            {
                throw new ArgumentException("Length or height cannot be negative.");
            }

            string line = "";
            for (int i = 1; i <= height; i++)
            {
                // Empty the line before writing a new one.
                line = "";

                // Construct a line.
                for (int j = 1; j <= length; j++)
                {
                    if (i == 1 || j == 1)
                    {
                        line += "# ";
                    }
                    else if (i == height || j == length)
                    {
                        line += "# ";
                    }
                    else
                    {
                        line += "  ";
                    }
                }
                // Write the constructed line.
                Console.WriteLine(line);
            }
        }

        public static void StartSquares()
        {
            string length = GetLength();
            string height = GetHeight();
            int lengthNum = 0;
            int heightNum = 0;

            // Attempt to convert user input to integer.
            try
            {
                lengthNum = Convert.ToInt32(length);
                heightNum = Convert.ToInt32(height);

                PrintSquare(lengthNum, heightNum);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Yellow Pages methods
        public static void Search(string input, List<string> partialMatch, List<string> fullMatch, List<string> names)
        {
            // Find and store matching names.
            foreach (string item in names)
            {
                // If the name matches completely we store the name in fullMatch and
                // dont need it in partial matches.
                if (item == input)
                {
                    fullMatch.Add(item);
                }
                else if (item.Contains(input))
                {
                    partialMatch.Add(item);
                }
            }
        }

        public static void PrintResults(List<string> partialMatch, List<string> fullMatch)
        {
            // Display both partially and fully matching names.
            Console.WriteLine("Partial matches:");
            foreach (string item in partialMatch)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\nFull matches:");
            foreach (string item in fullMatch)
            {
                Console.WriteLine(item);
            }
        }

        public static void StartYellowPages()
        { 
            // Search hits are cleared each iteration.
            partialMatch.Clear();
            fullMatch.Clear();

            Console.WriteLine("Search a name!");
            String input = Console.ReadLine();

            Search(input, partialMatch, fullMatch, names);

            PrintResults(partialMatch, fullMatch);
        }
        #endregion

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter <s> to create squares, or <yp> to access Yellow Pages.");
                string choice = Console.ReadLine();

                // Allow user to choose action
                if (choice == "s")
                {
                    StartSquares();
                }
                else if (choice == "yp")
                {
                    StartYellowPages();
                }
                else
                {
                    Console.WriteLine("Invalid choice.");
                }
            }
        }
    }
}